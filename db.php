<?php

namespace com\test\product;

class DbConnection
{

    private $mysqli;
    private static $instance;
    // the below approach makes sure the database connection does not get re-instantiated.
    // By simply saving the instance of the dbConnection inside the class along with
    // the connection only for one time.
    private function __construct()
    {
        $this->mysqli = new \mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        if (!$this->mysqli) {
            echo mysqli_connect_error();
        }
    }

    public static function getInstance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new dbConnection();
        }
        return self::$instance;
    }

    public function getConnection()
    {
        return $this->mysqli;
    }

    public function getProduct($type)
    {
        $result = $this->mysqli->query(
            sprintf(
                "SELECT * FROM `products` WHERE TYPE = '%s'",
                $this->mysqli->real_escape_string($type)
            )
        );
        if ($result->num_rows != "0") {
            $array = array();
            while ($obj = $result->fetch_object("com\\test\\product\\$type")) {
                $array[] = $obj;
            }
            return $array;
        }
        return [];
    }

    public function postProduct($data)
    {
        $result = $this->mysqli->query(
            sprintf(
                "INSERT INTO `products` (SKU,NAME,PRICE,TYPE,VALUE) VALUES ('%s','%s','%f','%s','%s')",
                $this->mysqli->real_escape_string($data['SKU']),
                $this->mysqli->real_escape_string($data['name']),
                $this->mysqli->real_escape_string($data['price']),
                $this->mysqli->real_escape_string($data['type']),
                $this->mysqli->real_escape_string($data['value'])
            )
        );
        if (!$result) {
            die(json_encode(mysqli_error($this->mysqli)));
        }
        echo json_encode($result);
    }

    public function deleteProduct($ID)
    {
        $result = $this->mysqli->query(
            "DELETE FROM `products` 
            WHERE PRODUCT_ID = "
            . $this->mysqli->real_escape_string($ID)
        );
        echo json_encode($result);
        exit;
    }
}

<?php

namespace com\test\product;

use com\test\Product\ProductType;

class Weight extends ProductType
{
    public $measurement = 'KG';
    protected function getProductType(): array
    {
        $dbConnection = dbConnection::getInstance();
        $result = $dbConnection->getProduct('Weight');
        return $result;
    }
}

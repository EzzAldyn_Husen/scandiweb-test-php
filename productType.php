<?php

namespace com\test\product;

use com\test\product\Size;
use com\test\product\Weight;
use com\test\product\Dimensions;

abstract class ProductType
{
    abstract protected function getProductType(): array;

    public static function getProducts()
    {
        $products = [new Size(), new Weight(), new Dimensions()];
        for ($index = 0; $index < count($products); $index++) {
            $products[$index] = $products[$index]->getProductType();
        }
        return $products;
    }
}

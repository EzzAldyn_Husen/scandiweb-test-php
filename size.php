<?php

namespace com\test\product;

use com\test\Product\ProductType;

class Size extends ProductType
{
    public $measurement = 'MB';
    protected function getProductType(): array
    {
        $dbConnection = dbConnection::getInstance();
        $result = $dbConnection->getProduct('Size');
        return $result;
    }
}

<?php

namespace com\test\product;

use com\test\Product\ProductType;

class Dimensions extends ProductType
{
    public $measurement = 'cm';
    protected function getProductType(): array
    {
        $dbConnection = dbConnection::getInstance();
        $result = $dbConnection->getProduct('Dimensions');
        return $result;
    }
}

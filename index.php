<?php

use com\test\product\DbConnection;
use com\test\Product\ProductType;

 require_once './config/config.ini.php';
 require_once './db.php';
 require_once './productType.php';
 require_once './size.php';
 require_once './weight.php';
 require_once './dimensions.php';

        header('Access-Control-Allow-Origin: https://scandiweb-product-app-react.herokuapp.com');
        header('Access-Control-Allow-Methods: POST, GET, PUT, OPTIONS, PATCH, DELETE');
        header('Access-Control-Allow-Credentials: true');
        header(
            'Access-Control-Allow-Headers: Authorization,Content-Type'
        );
        $rest_json = file_get_contents("php://input");
        $_POST = json_decode($rest_json, true);
        if ($_SERVER['REQUEST_METHOD'] === "GET") {
            $products = ProductType::getProducts();
            echo json_encode($products);
            exit;
        } elseif ($_SERVER['REQUEST_METHOD'] === "POST") {
            $dbConnection = DbConnection::getInstance();
            if (isset($_POST['DELETE'])) {
                foreach ($_POST['DELETE'] as $ID) {
                    $dbConnection->deleteProduct($ID);
                }
                echo json_encode(1);
                exit;
            }
            $dbConnection->postProduct($_POST);
            exit;
        }
